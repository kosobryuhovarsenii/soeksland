

window.addEventListener('scroll', function() {
  console.log(window.scrollY)
})

$(document).ready(function () {

  $(window).resize(function () {
    $(".viewport").attr("content", "width=" + (window.screen.width >= 480 ? "1440" : "device-width"));
  });

  $(".use-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    fade: true,
    speed: 500,
    prevArrow: $(".use-prev"),
    nextArrow: $(".use-next"),
    // autoplay: true,
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        },
      },
    ],
  });

  $(".product-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    fade: true,
    speed: 500,
    prevArrow: null,
    nextArrow: $(".product-next"),
    // autoplay: true,
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        },
      },
    ],
  });

  // $(".history-slider").owlCarousel({
  //   center: true,
  //   items: 5,
  //   loop: true,
  //   margin: 40,
  //   autoWidth: true,
  //   // responsive: {
  //   //   600: {
  //   //     items: 4,
  //   //   },
  //   // },
  // });

  $('.history-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: true,
    variableWidth: true,
    speed: 1000,
    prevArrow: $('.history-prev'),
    nextArrow: $('.history-next'),
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        }
      }
    ]
  })

  $(".header-menu a").click(function (e) {
    event.preventDefault(e);
    list(this);
    $("#menu-button").toggleClass("button-active");
    $("#menu-button").find("span").toggleClass("active");
    $(".header-menu").toggleClass("menu-active");
    $(".blur").fadeToggle(200);
  });

  $(".header .button").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".header-logo").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".banner-block a").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".product-slider .button").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".order-info .button").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".opt .button").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".title a").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".footer-logo").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  function list(el) {
    var elementClick = $(el).attr("href");
    var destination = $(elementClick).offset().top;
    $("html:not(:animated),body:not(:animated)").animate(
      {
        scrollTop: destination - $('header').height(),
      },
      1000
    );
    return false;
  }

  $("#menu-button").on("click", function () {
    $(this).toggleClass("button-active");
    $(this).find("span").toggleClass("active");
    $(".header-menu").toggleClass("menu-active");
    $(".blur").fadeToggle(200);
  });



  $(".blur").on("click", function () {
    $("#menu-button").toggleClass("button-active");
    $("#menu-button").find("span").toggleClass("active");
    $(".header-menu").toggleClass("menu-active");
    $(this).fadeToggle(200);
  });

  $('.opt-button').on('click', function() {
    $('.opt-block').toggleClass('opt-block__active')
    $(this).find('img').toggleClass('active')
  })

});
