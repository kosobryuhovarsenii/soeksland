var viewport = document.querySelector(".viewport");
viewport.setAttribute(
  "content",
  "width=" + (window.screen.width >= 480 ? "1440" : "device-width")
);
